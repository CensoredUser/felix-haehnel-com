# felixhaehnel.com website

This is a simple static site made with [Hugo](https://gohugo.io).
It's my personal blog and web presence.

This site is designed to be easy to work on, cheap to run, and fast.
