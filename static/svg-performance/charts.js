var data = [{
    protocol: "HTTP/1.1",
    scenario: "CSS3 Webfont",
    stage: "initial.har",
    bytes: 89151,
    time: 465.23291015625
}, {
    protocol: "HTTP/1.1",
    scenario: "CSS3 Webfont",
    stage: "reload-cached.har",
    bytes: 730,
    time: 179.406005859375
}, {
    protocol: "HTTP/1.1",
    scenario: "CSS3 Webfont",
    stage: "reload-changed.har",
    bytes: 7896,
    time: 196.7470703125
}, {
    protocol: "HTTP/1.1",
    scenario: "CSS background",
    stage: "initial.har",
    bytes: 721599,
    time: 6734.84008789062
}, {
    protocol: "HTTP/1.1",
    scenario: "CSS background",
    stage: "reload-cached.har",
    bytes: 190683,
    time: 6760.75903320312
}, {
    protocol: "HTTP/1.1",
    scenario: "CSS background",
    stage: "reload-changed.har",
    bytes: 191675,
    time: 6742.17700195312
}, {
    protocol: "HTTP/1.1",
    scenario: "<img>",
    stage: "initial.har",
    bytes: 715606,
    time: 6919.53588867187
}, {
    protocol: "HTTP/1.1",
    scenario: "<img>",
    stage: "reload-cached.har",
    bytes: 190681,
    time: 6994.92504882812
}, {
    protocol: "HTTP/1.1",
    scenario: "<img>",
    stage: "reload-changed.har",
    bytes: 191673,
    time: 6833.166015625
}, {
    protocol: "HTTP/1.1",
    scenario: "<object>",
    stage: "initial.har",
    bytes: 715714,
    time: 6479.90698242187
}, {
    protocol: "HTTP/1.1",
    scenario: "<object>",
    stage: "reload-cached.har",
    bytes: 190681,
    time: 8285.2490234375
}, {
    protocol: "HTTP/1.1",
    scenario: "<object>",
    stage: "reload-changed.har",
    bytes: 191673,
    time: 7783.291015625
}, {
    protocol: "HTTP/1.1",
    scenario: "SVG Sprite",
    stage: "initial.har",
    bytes: 146267,
    time: 2843.96997070312
}, {
    protocol: "HTTP/1.1",
    scenario: "SVG Sprite",
    stage: "reload-cached.har",
    bytes: 729,
    time: 230.162109375
}, {
    protocol: "HTTP/1.1",
    scenario: "SVG Sprite",
    stage: "reload-changed.har",
    bytes: 138911,
    time: 904.669921875
}, {
    protocol: "HTTP/1.1",
    scenario: "Inline <svg>",
    stage: "initial.har",
    bytes: 135123,
    time: 746.285888671875
}, {
    protocol: "HTTP/1.1",
    scenario: "Inline <svg>",
    stage: "reload-cached.har",
    bytes: 244,
    time: 61.947998046875
}, {
    protocol: "HTTP/1.1",
    scenario: "Inline <svg>",
    stage: "reload-changed.har",
    bytes: 134479,
    time: 728.39599609375
}, {
    protocol: "H2",
    scenario: "CSS3 Webfont",
    stage: "initial.har",
    bytes: 88522,
    time: 599.4609375
}, {
    protocol: "H2",
    scenario: "CSS3 Webfont",
    stage: "reload-cached.har",
    bytes: 11239,
    time: 211.660888671875
}, {
    protocol: "H2",
    scenario: "CSS3 Webfont",
    stage: "reload-changed.har",
    bytes: 109431,
    time: 703.385009765625
}, {
    protocol: "H2",
    scenario: "CSS background",
    stage: "initial.har",
    bytes: 531278,
    time: 6719.6630859375
}, {
    protocol: "H2",
    scenario: "CSS background",
    stage: "reload-cached.har",
    bytes: 530279,
    time: 6631.17895507812
}, {
    protocol: "H2",
    scenario: "CSS background",
    stage: "reload-changed.har",
    bytes: 530461,
    time: 6619.10498046875
}, {
    protocol: "H2",
    scenario: "<img>",
    stage: "initial.har",
    bytes: 524490,
    time: 6552.7939453125
}, {
    protocol: "H2",
    scenario: "<img>",
    stage: "reload-cached.har",
    bytes: 524210,
    time: 6689.63696289062
}, {
    protocol: "H2",
    scenario: "<img>",
    stage: "reload-changed.har",
    bytes: 524326,
    time: 6648.38110351562
}, {
    protocol: "H2",
    scenario: "<object>",
    stage: "initial.har",
    bytes: 538911,
    time: 6808.19702148437
}, {
    protocol: "H2",
    scenario: "<object>",
    stage: "reload-cached.har",
    bytes: 538904,
    time: 8677.2880859375
}, {
    protocol: "H2",
    scenario: "<object>",
    stage: "reload-changed.har",
    bytes: 538990,
    time: 8535.88696289062
}, {
    protocol: "H2",
    scenario: "SVG Sprite",
    stage: "initial.har",
    bytes: 145617,
    time: 2398.13208007812
}, {
    protocol: "H2",
    scenario: "SVG Sprite",
    stage: "reload-cached.har",
    bytes: 145646,
    time: 862.89501953125
}, {
    protocol: "H2",
    scenario: "SVG Sprite",
    stage: "reload-changed.har",
    bytes: 144973,
    time: 933.794921875
}, {
    protocol: "H2",
    scenario: "Inline <svg>",
    stage: "initial.har",
    bytes: 134951,
    time: 724.31396484375
}, {
    protocol: "H2",
    scenario: "Inline <svg>",
    stage: "reload-cached.har",
    bytes: 134888,
    time: 721.76611328125
}, {
    protocol: "H2",
    scenario: "Inline <svg>",
    stage: "reload-changed.har",
    bytes: 134285,
    time: 725.778076171875
}];

function byScenarioProtocol(a, b){
    var scen = a.scenario.localeCompare(b.scenario);
    return scen ===0 ? a.protocol.localeCompare(b.protocol): scen;
} 

function byStage(stage) {
    return function(val) {
        return val.stage === stage;
    }
}

function options(label, data, color, metric) {
    return {
        type: 'horizontalBar',
        data: {
            labels: data.map(function(val) {
                return val.scenario + ' (' + val.protocol + ')'
            }),
            datasets: [
                {
                    label: label,
                    backgroundColor: color,
                    data: data.map(function(val) {
                        return val[metric]
                    })
                }
            ]
        }
    };
}

var initial = data.filter(byStage('initial.har')).sort(byScenarioProtocol);
var reload = data.filter(byStage('reload-cached.har')).sort(byScenarioProtocol);
var change = data.filter(byStage('reload-changed.har')).sort(byScenarioProtocol);

new Chart(document.getElementById("initial-load-chart"), options('Initial load in ms', initial, '#FF0000', 'time'));
new Chart(document.getElementById("reload-load-chart"), options('Cached reload in ms', reload, '#FF0000', 'time'));
new Chart(document.getElementById("modify-load-chart"), options('Cached reload with modification in ms', change, '#FF0000', 'time'));

new Chart(document.getElementById("initial-byte-chart"), options('Initial load in ms', initial, '#FF0000', 'bytes'));
new Chart(document.getElementById("reload-byte-chart"), options('Cached reload in ms', reload, '#FF0000', 'bytes'));
new Chart(document.getElementById("modify-byte-chart"), options('Cached reload with modification in ms', change, '#FF0000', 'bytes'));