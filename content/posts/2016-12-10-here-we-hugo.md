---
title: "Here we (Hu)go!"
description: "A brave new static world."
date: 2016-12-10
---

I've moved my blog. The old `felixhaehnel.com` blog was powered by a standalone 
Wordpress install running in Azure. 

This was: 

- expensive
- pointless
- slow
- constantly unsecure
- slow
- slow


So I decided to bite the bullet and look for a better CMS. What I discovered was: 
I don't want another CMS. I want to manage and build my site locally. You know, like 
in the old days. Except not quite like in the old days; I don't want to mess with HTML and 
CSS just to put up some new content. I don't want to have to FTP into a server provided by
my crappy ISP to update the page. 

I want all the new buzzwords: Containers, Continuous Deployments, and _static content_.

Summing up my new deployment in one image:
{{< figure src="//i.giphy.com/12NUbkX6p4xOO4.gif" width="100%" >}} 


So here's the setup now: 

- [Hugo](https://gohugo.io) to build the site
- [Hugo Cactus Theme](https://github.com/digitalcraftsman/hugo-cactus-theme) to style the site
- [Bitbucket](https://bitbucket.org/censoreduser/felix-haehnel-com) to host the site
- [Bitbucket Pipelines](https://bitbucket.org/product/features/pipelines) to deploy the site
- [AWS S3](https://aws.amazon.com/s3) to host the site
- [AWS CloudFront](https://aws.amazon.com/cloudfront) as a CDN

My new workflow looks like this: 

- Run `hugo server` in the root of the repo directory
- Make some changes, mostly by writing `Markdown` files
- Commit and push (since it's just me working on this everything is straight to `master` #yolo)
- (10s of elevator music)
- felixhaehnel.com is updated

This is, in my mind, how deploying a static site or blog should work. It's simple enough I can add
whatever content I want in seconds, but flexible enough so I can mess around with the guts of the page
if I so choose. 

The source to this page is available in a [Bitbucket repository](https://bitbucket.org/CensoredUser/felix-haehnel-com/)
for anyone to peruse and/or copy.

This is my first adventure into this brave new static world. So far, so good.
