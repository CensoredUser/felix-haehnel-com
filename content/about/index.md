+++
date = "2015-06-20T14:02:37+02:00"
title = "About"
hidden = true
+++

I’m a Software Developer from Sydney, now living in New York.
I enjoy Mountain Biking, Motorcycles, Surfing, Snowboarding, travelling and eating, among other things.
I may occasionally blog about my adventures, coding projects and random thoughts here.
